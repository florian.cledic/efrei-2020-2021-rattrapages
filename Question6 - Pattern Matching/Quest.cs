﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;

namespace Question6
{
    class Quest
    {
        public IReadOnlyList<Soldier> Soldiers { get; }
        public Environment Environment { get; }

        public Quest(IReadOnlyList<Soldier> soldiers, Environment environment)
        {
            Soldiers = soldiers;
            Environment = environment;
        }

        public double ComputePrice()
        {
            return Soldiers.Sum(GetRate) * 10;
        }

        public double GetRate(Soldier soldier)
        {
            return soldier switch
            {
                // vvv Insérez votre solution ci-dessous vvv
                
                ElvenSoldier =>
            {
                if (Environment.Forest)
                    return 0.8;
            },

            DwarfSoldier =>
            {
                
            },

            KoboldGroup =>
            {
                
            },

                _ => throw new NotImplementedException()
                // ^^^ Insérez votre solution ci-dessus  ^^^
            };
        }
    }

}
