﻿using System;
using System.Threading.Tasks;

namespace Question7
{
    class Program
    {
        static readonly Random random = new Random();

        static async Task Start_Do_600_Mi_Async()
        {
            Console.WriteLine("Do");
            await Task.Delay(600);
            Console.WriteLine("Mi");
        }
        static async Task Start_200_Re_Async()
        {
            await Task.Delay(200);
            Console.WriteLine("Re");
        }
        static async Task Start_Fa_400Task_Si_Async(Task otherTask)
        {
            Console.WriteLine("Fa");
            await Task.WhenAll(Task.Delay(400), otherTask);
            Console.WriteLine("Si");
        }
        static async Task Start_100_random_La_Async()
        {
            await Task.Delay(100 + random.Next(800));
            Console.WriteLine("La");
        }
        static async Task Start_400_Sol_Async()
        {
            await Task.Delay(200);
            Console.WriteLine("Sol");
        }

        static async Task Main()
        {
            // vvv Insérez votre solution ci-dessous vvv
            Start_200_Re_Async();
            await Start_Do_600_Mi_Async();
            Start_400_Sol_Async();
            await Start_Fa_400Task_Si_Async(Start_100_random_La_Async());
            // ^^^ Insérez votre solution ci-dessus  ^^^
        }

        // vvv Si nécessaire, vous pouvez créer de nouvelles méthodes ci-dessous vvv

        // ^^^ Si nécessaire, vous pouvez créer de nouvelles méthodes ci-dessus  ^^^
    }
}
