﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Question8.Domain;
using Question8.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Question8.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class RulesController : ControllerBase
    {
        private readonly IRuleRepository repository;
        
        public RulesController(IRuleRepository repository)
        {
            this.repository = repository;
            
        }
        
        [HttpGet("{thing}")]
        public ActionResult<RuleForThings> GetThing(string thing)
        {
            var ruleOfThink = repository.GetRule(thing);

            if (ruleOfThink != null)
            {
                return Ok(repository.GetRule(thing));
            }
            return NotFound("Error : thing not found");
            
        }
        
        [HttpPut("{thing}")]
        public ActionResult<RuleForThings> PutThing(RuleForThings ruleForThings)
        {
            if (ruleForThings.Behavior == null || ruleForThings.Thing == null)
            {
                return BadRequest("Error : bad body");
            }
            var ruleOfThinkAtCompare = repository.GetRule(ruleForThings.Thing);

            if (ruleOfThinkAtCompare == null)
            {
                repository.StoreRule(ruleForThings);
            }
            else
            {
                repository.DeleteRule(ruleForThings.Thing);
                repository.StoreRule(ruleForThings);
            }

            return Ok(ruleForThings);

        }
        
    }
}
