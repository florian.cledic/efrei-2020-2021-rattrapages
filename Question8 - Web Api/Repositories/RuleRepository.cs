﻿using Question8.Domain;
using System.Collections.Generic;

namespace Question8.Repositories
{
    public class RuleRepository : IRuleRepository
    {
        private readonly Dictionary<string, RuleForThings> rules = new Dictionary<string, RuleForThings>();

        public RuleForThings GetRule(string thing)
            => rules.TryGetValue(thing, out var rule)
            ? rule
            : null;

        public void StoreRule(RuleForThings rule)
            => rules[rule.Thing] = rule;

        public void DeleteRule(string thing)
            => rules.Remove(thing);
    }
}
