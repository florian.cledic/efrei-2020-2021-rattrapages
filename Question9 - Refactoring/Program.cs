﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Question9
{
    class Program
    {
        static void Main(string[] args)
        {
            var studentFileContent = @"12,C#,John Doe
11,Java,Jean Dupuis
14,C#,Jane Doe
10,C#,Jack Smith
15,Java,Jacques Dupont";

            var classes = ParseStudentFile(studentFileContent);

            foreach (var studentClass in classes)
            {
                Console.WriteLine("====================");
                Console.WriteLine($"> {studentClass.Name}:");
                Console.WriteLine("--------------------");

                foreach (var student in studentClass.Students)
                {
                    Console.WriteLine($" * {student.Name} ({student.Id})");
                }
                Console.WriteLine();
            }
        }

        public class Student
        {
            public int Id { get; }
            public string Name { get; }

            public Student(int id, string name)
            {
                Id = id;
                Name = name;
            }
        }

        public class StudentClass
        {
            public string Name { get; }
            public IReadOnlyList<Student> Students { get; }

            public StudentClass(string name, IEnumerable<Student> students)
            {
                Name = name;
                Students = students.ToList();
            }
        }


        static IReadOnlyList<StudentClass> ParseStudentFile(string studentData)
        {
            // vvv Refactores le code ci-dessous vvv
            Dictionary<string, List<Student>> groupedStudents = new Dictionary<string, List<Student>>();
            var lines = studentData.Replace("\r\n", "\n").Split('\n');
            foreach (var line in lines)
            {
                var parts = studentData.Split(',', 3);

                if (!int.TryParse(parts[0], out var id))
                    throw new InvalidOperationException($"Expected {parts[0]} to be an integer id");

                var className = parts[1];
                var studentName = parts[2];

                if (!groupedStudents.ContainsKey(className))
                    groupedStudents.Add(className, new List<Student>());

                groupedStudents[className].Add(new Student(id, studentName));
            }

            List<StudentClass> result = new List<StudentClass>();
            foreach (var (className, students) in groupedStudents)
            {
                result.Add(new StudentClass(className, students));
            }

            return result;
            // ^^^ Refactores le code ci-dessus  ^^^
        }

        // vvv Si nécessaire, vous pouvez créer de nouvelles méthodes ci-dessous vvv

        // ^^^ Si nécessaire, vous pouvez créer de nouvelles méthodes ci-dessus  ^^^
    }
}
